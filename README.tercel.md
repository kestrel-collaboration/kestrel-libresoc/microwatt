# ARCHITECTURE

Tercel is a Wishbone-compatible, 32-bit, single+quad SPI Flash controller with XIP support.  Both 3BA and 4BA devices are supported.

Tercel provides two interfaces to the host CPU:
1. A direct MMIO read/write region for the flash device
2. A configuration space where the core can be reconfigured on-line for operation beyond single SPI 3BA mode with fallback clocks (default)

# USAGE

## General Usage

On reset, the Tercel core provides basic read/write access in single SPI, 3BA mode to any attached Flash device.  It uses the generic well-known single byte access instructions to provide full XIP support.  Host software is responsible for reading the Flash ID of the attached Flash device and reconfiguring the Tercel core for faster and more advanced operating modes.  This reconfiguration can take place online, with no interruption to the concurrent read operations in progress on the main MMIO Flash window.

By default, with Microwatt, the two bus regions are available at:
Flash MMIO (XIP base):	0xf0000000
Core configuration:	0xc8050000

## Read Flash ID

 - Enable user mode
   Set bit 0 of "Core control register 1"

 - Send Flash ID command
   Write 0x9e to SPI MMIO base address (offset 0x0)

 - Read response
   Read four bytes from SPI MMIO base address (offset 0x0) and assemble into 32-bit device ID
   Read sequence is big endian per Flash device convention

 - Disable user mode
   Clear bit 0 of "Core control register 1"

# REGISTER MAP

## [0x00 - 0x07] Device ID

 Device make/model unique identifier for PnP functionality
 Fixed value: 0x7c5250545350494d

## [0x08 - 0x0b] Device version

 Device revision (stepping)

 | Bits  | Description   |
 |-------|---------------|
 | 31:16 | Major version |
 | 15:8  | Minor version |
 | 7:0   | Patch level   |

## [0x0c - 0x0f] System clock frequency

 Can be used to set divisor to meet specific SPI Flash clock frequency requirements

## [0x10 - 0x13] PHY configuration register 1

 Default: 0x00000a10

 | Bits  | Description                                                                                            |
 |-------|--------------------------------------------------------------------------------------------------------|
 | 31:24 | Insert idle cycles with CS deasserted between SPI operations (cycle count to insert, 0 for none)       |
 | 23:22 | Reserved                                                                                               |
 | 21    | Enable quad I/O data write in QSPI mode                                                                |
 | 20    | Enable quad I/O data read in QSPI mode                                                                 |
 | 19    | Enable fast reads (1 == use fast read commands and cycles, 0 == use standard read commands and cycles) |
 | 18    | Enable 4BA addressing mode (1 == 4BA, 0 == 3BA)                                                        |
 | 17:16 | PHY I/O type (0 == single, 2 == quad, others invalid)                                                  |
 | 15:8  | Dummy cycle count                                                                                      |
 | 7:0   | SPI clock divisor                                                                                      |

 Clock divisor works as follows:
  Clock frequency calculation:
   spi_clock_frequency = peripheral_bus_clock_frequency / ((spi_clock_divisor - 1) * 2)

 | Clock divisor value | Actual division                    |
 |---------------------|------------------------------------|
 | 0                   | override to standard divide by two |
 | 1                   | divide by 1                        |
 | 2                   | divide by 2                        |
 | 3                   | divide by 4                        |
 | 4                   | divide by 6                        |
 | 5                   | divide by 8                        |
 | 6                   | divide by 10                       |
 | 7                   | divide by 12                       |
 | ...                 | ...                                |

## [0x14 - 0x17] Flash configuration register 1

  Default: 0x13031303

 | Bits  | Description           |
 |-------|-----------------------|
 | 31:24 | Reserved              |
 | 23:16 | QSPI 3BA read command |
 | 15:8  | SPI 4BA read command  |
 | 7:0   | SPI 3BA read command  |

## [0x18 - 0x1b] Flash configuration register 2

  Default: 0xeceb0c0b

 | Bits  | Description                |
 |-------|----------------------------|
 | 31:24 | QSPI 4BA fast read command |
 | 23:16 | QSPI 3BA fast read command |
 | 15:8  | SPI 4BA fast read command  |
 | 7:0   | SPI 3BA fast read command  |

## [0x1c - 0x1f] Flash configuration register 3

  Default: 0x34321202

 | Bits  | Description              |
 |-------|--------------------------|
 | 31:24 | QSPI 4BA program command |
 | 23:16 | QSPI 3BA program command |
 | 15:8  | SPI 4BA program command  |
 | 7:0   | SPI 3BA program command  |

## [0x20 - 0x23] Flash configuration register 4

  Default: 0x00000000

  Cycles to keep CS asserted after operation completion.  Used to support high-throughput multi-cycle transfers with specific Flash devices.

  See also "Flash configuration register 5"

## [0x24 - 0x27] Flash configuration register 5

  Default: 0x00000000

 | Bits | Description             |
 |------|-------------------------|
 | 31:2 | Reserved                |
 | 1    | Allow multicycle writes |
 | 0    | Allow multicycle reads  |

## [0x28 - 0x2b] Core control register 1

  Default: 0x00000000

 | Bits | Description |                          |
 |------|-------------|--------------------------|
 |      | 31:1        | Reserved                 |
 |      | 0           | User command mode enable |

 User command mode operates in conjunction with "Core data register 1" to support custom (non-data-I/O) SPI commands.

## [0x2c - 0x2f] Core data register 1

 Data transfer to/from SPI device in user command mode

 See also "Core control register 1"

# LICENSE

Tercel is licensed under the terms of the GNU LGPLv3+.  See LICENSE.tercel for details.

# DOCUMENTATION CREDITS

(c) 2022 Raptor Engineering, LLC
