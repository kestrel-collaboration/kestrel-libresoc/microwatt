//-----------------------------------------------------------------------------
// Copyright 2017 Damien Pretet ThotIP
// Copyright 2020 Raptor Engineering, LLC
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//-----------------------------------------------------------------------------  

`timescale 1 ns / 1 ps
`default_nettype none

module wptr_full

    #(
    parameter ADDRSIZE = 4
    )(
    input  wire                wclk,
    input  wire                wrst_n,
    input  wire                winc,
    input  wire [ADDRSIZE  :0] wq2_rptr,
    output reg                 wfull,
    output reg                 awfull,
    output wire [ADDRSIZE-1:0] waddr,
    output reg  [ADDRSIZE  :0] wptr,

    output wire [ADDRSIZE  :0] wq2_rptr_binary,
    );

    reg  [ADDRSIZE:0] wbin;
    wire [ADDRSIZE:0] wgraynext, wbinnext, wgraynextp1;
    wire              awfull_val, wfull_val;
    reg  [ADDRSIZE:0] wq2_rptr_reg = 0;

    // GRAYSTYLE2 pointer
    always @(posedge wclk or negedge wrst_n) begin

        if (!wrst_n) 
            {wbin, wptr} <= 0;
        else         
            {wbin, wptr} <= {wbinnext, wgraynext};

    end
    
    // Memory write-address pointer (okay to use binary to address memory)
    assign waddr = wbin[ADDRSIZE-1:0];
    assign wbinnext  = wbin + (winc & ~wfull);
    assign wgraynext = (wbinnext >> 1) ^ wbinnext;
    assign wgraynextp1 = ((wbinnext + 1'b1) >> 1) ^ (wbinnext + 1'b1);
    
    //------------------------------------------------------------------ 
    // Simplified version of the three necessary full-tests:
    // assign wfull_val=((wgnext[ADDRSIZE] !=wq2_rptr[ADDRSIZE] ) &&
    //                   (wgnext[ADDRSIZE-1]  !=wq2_rptr[ADDRSIZE-1]) &&
    // (wgnext[ADDRSIZE-2:0]==wq2_rptr[ADDRSIZE-2:0])); 
    //------------------------------------------------------------------
    
     assign wfull_val = (wgraynext == {~wq2_rptr[ADDRSIZE:ADDRSIZE-1],wq2_rptr[ADDRSIZE-2:0]});
     assign awfull_val = (wgraynextp1 == {~wq2_rptr[ADDRSIZE:ADDRSIZE-1],wq2_rptr[ADDRSIZE-2:0]});

    generate genvar i;
        for (i=0; i<ADDRSIZE+1; i=i+1) begin : gen_bin
            assign wq2_rptr_binary[i] = ^wq2_rptr_reg[ADDRSIZE:i];
        end
    endgenerate

     always @(posedge wclk or negedge wrst_n) begin

        if (!wrst_n) begin
            awfull <= 1'b0;
            wfull  <= 1'b0;
            wq2_rptr_reg <= 0;
        end
        else begin
            awfull <= awfull_val;
            wfull  <= wfull_val;
            wq2_rptr_reg <= wq2_rptr;
        end

    end

endmodule

`resetall
