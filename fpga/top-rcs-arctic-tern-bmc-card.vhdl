library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.wishbone_types.all;

entity toplevel is
    generic (
        MEMORY_SIZE        : integer  := 16384;
        RAM_INIT_FILE      : string   := "firmware.hex";
        RESET_LOW          : boolean  := true;
        CLK_INPUT          : positive := 125000000;
        CLK_FREQUENCY      : positive := 48000000;
        HAS_FPU            : boolean  := true;
        HAS_BTC            : boolean  := false;
        USE_LITEDRAM       : boolean  := true;
        NO_BRAM            : boolean  := true;
        SCLK_STARTUPE2     : boolean := false;
        SPI_FLASH_OFFSET   : integer := 4194304;
        SPI_FLASH_DEF_CKDV : natural := 1;
        SPI_FLASH_DEF_QUAD : boolean := true;
        LOG_LENGTH         : natural := 0;
        USE_LITEETH        : boolean  := true;
        USE_TERCEL         : boolean  := true;
        USE_LPC_SLAVE      : boolean  := true;
        UART_IS_16550      : boolean  := true;
        HAS_UART1          : boolean  := false;
        ICACHE_NUM_LINES   : natural := 64
        );
    port(
        ext_clk   : in  std_ulogic;
        ext_rst_n : in  std_ulogic;

        -- UART0 signals:
        uart0_txd : out std_ulogic;
        uart0_rxd : in  std_ulogic;

        -- SPI
        spi_flash_cs_n   : out std_ulogic;
        spi_flash_mosi   : inout std_ulogic;
        spi_flash_miso   : inout std_ulogic;
        spi_flash_wp_n   : inout std_ulogic;
        spi_flash_hold_n : inout std_ulogic;

        -- LPC slave
        lpc_slave_data     : inout std_ulogic_vector(3 downto 0);
        lpc_slave_serirq   : inout std_ulogic;
        lpc_slave_frame_n  : in std_ulogic;
        lpc_slave_reset_n  : in std_ulogic;
        lpc_slave_clock    : in std_ulogic;

        -- Ethernet
        eth_clocks_tx    : out std_ulogic;
        eth_clocks_rx    : in std_ulogic;
        eth_mdio         : inout std_ulogic;
        eth_mdc          : out std_ulogic;
        eth_rx_ctl       : in std_ulogic;
        eth_rx_data      : in std_ulogic_vector(3 downto 0);
        eth_tx_ctl       : out std_ulogic;
        eth_tx_data      : out std_ulogic_vector(3 downto 0);

        -- DRAM wires
        ddram_a       : out std_ulogic_vector(13 downto 0);
        ddram_ba      : out std_ulogic_vector(2 downto 0);
        ddram_ras_n   : out std_ulogic;
        ddram_cas_n   : out std_ulogic;
        ddram_we_n    : out std_ulogic;
        ddram_cs_n    : out std_ulogic;
        ddram_dm      : out std_ulogic_vector(3 downto 0);
        ddram_dq      : inout std_ulogic_vector(31 downto 0);
        ddram_dqs_p   : inout std_ulogic_vector(3 downto 0);
        ddram_clk_p   : out std_ulogic_vector(1 downto 0);
        -- only the positive differential pin is instantiated
        --ddram_dqs_n   : inout std_ulogic_vector(3 downto 0);
        --ddram_clk_n   : out std_ulogic_vector(1 downto 0);
        ddram_cke     : out std_ulogic;
        ddram_odt     : out std_ulogic;
        ddram_reset_n : out std_ulogic
        );
end entity toplevel;

architecture behaviour of toplevel is

    -- Reset signals:
    signal soc_rst : std_ulogic;
    signal pll_rst : std_ulogic;

    -- Internal clock signals:
    signal system_clk        : std_ulogic;
    signal system_clk_locked : std_ulogic;

    -- External IOs from the SoC
    signal wb_ext_io_in        : wb_io_master_out;
    signal wb_ext_io_out       : wb_io_slave_out;
    signal wb_ext_is_dram_csr  : std_ulogic;
    signal wb_ext_is_dram_init : std_ulogic;
    signal wb_ext_is_eth       : std_ulogic;
    signal wb_ext_is_tercel    : std_ulogic;
    signal wb_ext_is_lpc_slave : std_ulogic;

    -- DRAM main data wishbone connection
    signal wb_dram_in          : wishbone_master_out;
    signal wb_dram_out         : wishbone_slave_out;

    -- DRAM control wishbone connection
    signal wb_dram_ctrl_out    : wb_io_slave_out := wb_io_slave_out_init;

    -- LiteEth connection
    signal ext_irq_eth         : std_ulogic;
    signal wb_eth_out          : wb_io_slave_out := wb_io_slave_out_init;

    -- Tercel connection
    signal wb_tercel_out       : wb_io_slave_out := wb_io_slave_out_init;

    -- Aquila connection
    signal wb_lpc_slave_out    : wb_io_slave_out := wb_io_slave_out_init;

    -- Control/status
    signal core_alt_reset : std_ulogic;

    -- SPI flash
    signal spi_sck     : std_ulogic;
    signal spi_cs_n    : std_ulogic;
    signal spi_sdat_o  : std_ulogic_vector(3 downto 0);
    signal spi_sdat_oe : std_ulogic_vector(3 downto 0);
    signal spi_sdat_i  : std_ulogic_vector(3 downto 0);

    -- LPC slave bus
    signal lpc_sdat_o   : std_ulogic_vector(3 downto 0);
    signal lpc_slave_oe : std_ulogic;
    signal lpc_sdat_i   : std_ulogic_vector(3 downto 0);
    signal lpc_sirq_o   : std_ulogic;
    signal lpc_sirq_oe  : std_ulogic;
    signal lpc_sirq_i   : std_ulogic;
    signal lpc_sframe_n : std_ulogic;
    signal lpc_sreset_n : std_ulogic;
    signal lpc_sclock   : std_ulogic;

    -- SPI main data wishbone connection
    signal wb_spiflash_in      : wb_io_master_out;
    signal wb_spiflash_out     : wb_io_slave_out;

    signal wb_master_lpc_slave_in  : wishbone_slave_out;
    signal wb_master_lpc_slave_out : wishbone_master_out;

    -- Fixup various memory sizes based on generics
    function get_bram_size return natural is
    begin
        if USE_LITEDRAM and NO_BRAM then
            return 0;
        else
            return MEMORY_SIZE;
        end if;
    end function;

    function get_payload_size return natural is
    begin
        if USE_LITEDRAM and NO_BRAM then
            return MEMORY_SIZE;
        else
            return 0;
        end if;
    end function;

    constant BRAM_SIZE    : natural := get_bram_size;
    constant PAYLOAD_SIZE : natural := get_payload_size;

    COMPONENT USRMCLK
        PORT(
            USRMCLKI : IN STD_ULOGIC;
            USRMCLKTS : IN STD_ULOGIC
        );
    END COMPONENT;

    attribute syn_noprune: boolean ;
    attribute syn_noprune of USRMCLK: component is true;

    component aquila_lpc_sdr_tristate
        port(
            i   : out std_ulogic;
            oe  : in std_ulogic;
            o   : in std_ulogic;
            clk : in std_ulogic;
            p   : inout std_ulogic
        );
    end component;
    attribute syn_noprune of aquila_lpc_sdr_tristate: component is true;

begin

    -- Main SoC
    soc0: entity work.soc
        generic map(
            MEMORY_SIZE        => BRAM_SIZE,
            RAM_INIT_FILE      => RAM_INIT_FILE,
            SIM                => false,
            CLK_FREQ           => CLK_FREQUENCY,
            HAS_FPU            => HAS_FPU,
            HAS_BTC            => HAS_BTC,
            HAS_DRAM           => USE_LITEDRAM,
            DRAM_SIZE          => 256 * 1024 * 1024,
            DRAM_INIT_SIZE     => PAYLOAD_SIZE,
            HAS_SPI_FLASH      => true,
            SPI_FLASH_DLINES   => 4,
            SPI_FLASH_OFFSET   => SPI_FLASH_OFFSET,
            SPI_FLASH_DEF_CKDV => SPI_FLASH_DEF_CKDV,
            SPI_FLASH_DEF_QUAD => SPI_FLASH_DEF_QUAD,
            LOG_LENGTH         => LOG_LENGTH,
            HAS_LITEETH        => USE_LITEETH,
            HAS_TERCEL         => USE_TERCEL,
            HAS_LPC_SLAVE      => USE_LPC_SLAVE,
            UART0_IS_16550     => UART_IS_16550,
            HAS_UART1          => HAS_UART1,
            ICACHE_NUM_LINES   => ICACHE_NUM_LINES
            )
        port map (
            -- System signals
            system_clk        => system_clk,
            rst               => soc_rst,

            -- UART signals
            uart0_txd         => uart0_txd,
            uart0_rxd         => uart0_rxd,

            -- DRAM wishbone
            wb_dram_in           => wb_dram_in,
            wb_dram_out          => wb_dram_out,

            -- External interrupts
            ext_irq_eth       => ext_irq_eth,

            -- IO wishbone
            wb_ext_io_in         => wb_ext_io_in,
            wb_ext_io_out        => wb_ext_io_out,
            wb_ext_is_dram_csr   => wb_ext_is_dram_csr,
            wb_ext_is_dram_init  => wb_ext_is_dram_init,
            wb_ext_is_eth        => wb_ext_is_eth,
            wb_ext_is_tercel     => wb_ext_is_tercel,
            wb_ext_is_lpc_slave  => wb_ext_is_lpc_slave,

            wb_spiflash_in       => wb_spiflash_in,
            wb_spiflash_out      => wb_spiflash_out,

            wb_lpc_slave_in      => wb_master_lpc_slave_in,
            wb_lpc_slave_out     => wb_master_lpc_slave_out,

            alt_reset            => core_alt_reset
            );

    nodram: if not USE_LITEDRAM generate
        signal ddram_clk_dummy : std_ulogic;
    begin
        reset_controller: entity work.soc_reset
            generic map(
                RESET_LOW => RESET_LOW
                )
            port map(
                ext_clk => ext_clk,
                pll_clk => system_clk,
                pll_locked_in => system_clk_locked,
                ext_rst_in => ext_rst_n,
                pll_rst_out => pll_rst,
                rst_out => soc_rst
                );

        clkgen: entity work.clock_generator
            generic map(
                CLK_INPUT_HZ => CLK_INPUT,
                CLK_OUTPUT_HZ => CLK_FREQUENCY
                )
            port map(
                ext_clk => ext_clk,
                pll_rst_in => pll_rst,
                pll_clk_out => system_clk,
                pll_locked_out => system_clk_locked
                );

        core_alt_reset <= '0';

    end generate;

    has_dram: if USE_LITEDRAM generate
        signal dram_init_done  : std_ulogic;
        signal dram_init_error : std_ulogic;
        signal dram_sys_rst    : std_ulogic;
        signal rst_gen_rst     : std_ulogic;
    begin

        -- Eventually dig out the frequency from
        -- litesdram generate.py sys_clk_freq
        -- but for now, assert it's 48Mhz for Arctic Tern
        assert CLK_FREQUENCY = 48000000;

        reset_controller: entity work.soc_reset
            generic map(
                RESET_LOW => RESET_LOW,
                PLL_RESET_BITS => 18,
                SOC_RESET_BITS => 1
                )
            port map(
                ext_clk => ext_clk,
                pll_clk => system_clk,
                pll_locked_in => system_clk_locked,
                ext_rst_in => ext_rst_n,
                pll_rst_out => pll_rst,
                rst_out => rst_gen_rst
                );

        -- Generate SoC reset
        soc_rst_gen: process(system_clk)
        begin
            if ext_rst_n = '0' then
                soc_rst <= '1';
            elsif rising_edge(system_clk) then
                soc_rst <= dram_sys_rst or not system_clk_locked;
            end if;
        end process;

        dram: entity work.litedram_wrapper
            generic map(
                DRAM_ABITS => 24,
                DRAM_ALINES => 14,
                DRAM_DLINES => 32,
                DRAM_CKLINES => 2,
                DRAM_PORT_WIDTH => 128,
                NUM_LINES => 8, -- reduce from default of 64 to make smaller/timing
                PAYLOAD_FILE => RAM_INIT_FILE,
                PAYLOAD_SIZE => PAYLOAD_SIZE
                )
            port map(
                clk_in          => ext_clk,
                rst             => pll_rst,
                system_clk      => system_clk,
                system_reset    => dram_sys_rst,
                core_alt_reset  => core_alt_reset,
                pll_locked      => system_clk_locked,

                wb_in           => wb_dram_in,
                wb_out          => wb_dram_out,
                wb_ctrl_in      => wb_ext_io_in,
                wb_ctrl_out     => wb_dram_ctrl_out,
                wb_ctrl_is_csr  => wb_ext_is_dram_csr,
                wb_ctrl_is_init => wb_ext_is_dram_init,

                init_done       => dram_init_done,
                init_error      => dram_init_error,

                ddram_a         => ddram_a,
                ddram_ba        => ddram_ba,
                ddram_ras_n     => ddram_ras_n,
                ddram_cas_n     => ddram_cas_n,
                ddram_we_n      => ddram_we_n,
                ddram_cs_n      => ddram_cs_n,
                ddram_dm        => ddram_dm,
                ddram_dq        => ddram_dq,
                ddram_dqs_p     => ddram_dqs_p,
                ddram_clk_p     => ddram_clk_p,
                -- only the positive differential pin is instantiated
                --ddram_dqs_n     => ddram_dqs_n,
                --ddram_clk_n     => ddram_clk_n,
                ddram_cke       => ddram_cke,
                ddram_odt       => ddram_odt,

                ddram_reset_n   => ddram_reset_n
                );

    end generate;

    has_liteeth : if USE_LITEETH generate

        component liteeth_core port (
            sys_clock           : in std_ulogic;
            sys_reset           : in std_ulogic;
            rgmii_eth_clocks_tx : out std_ulogic;
            rgmii_eth_clocks_rx : in std_ulogic;
            rgmii_eth_mdio      : inout std_ulogic;
            rgmii_eth_mdc       : out std_ulogic;
            rgmii_eth_rx_ctl    : in std_ulogic;
            rgmii_eth_rx_data   : in std_ulogic_vector(3 downto 0);
            rgmii_eth_tx_ctl    : out std_ulogic;
            rgmii_eth_tx_data   : out std_ulogic_vector(3 downto 0);
            wishbone_adr        : in std_ulogic_vector(29 downto 0);
            wishbone_dat_w      : in std_ulogic_vector(31 downto 0);
            wishbone_dat_r      : out std_ulogic_vector(31 downto 0);
            wishbone_sel        : in std_ulogic_vector(3 downto 0);
            wishbone_cyc        : in std_ulogic;
            wishbone_stb        : in std_ulogic;
            wishbone_ack        : out std_ulogic;
            wishbone_we         : in std_ulogic;
            wishbone_cti        : in std_ulogic_vector(2 downto 0);
            wishbone_bte        : in std_ulogic_vector(1 downto 0);
            wishbone_err        : out std_ulogic;
            interrupt           : out std_ulogic
            );
        end component;

        signal wb_eth_cyc     : std_ulogic;
        signal wb_eth_adr     : std_ulogic_vector(29 downto 0);

    begin
        liteeth :  liteeth_core
            port map(
                sys_clock           => system_clk,
                sys_reset           => soc_rst,
                rgmii_eth_clocks_tx => eth_clocks_tx,
                rgmii_eth_clocks_rx => eth_clocks_rx,
                rgmii_eth_mdio      => eth_mdio,
                rgmii_eth_mdc       => eth_mdc,
                rgmii_eth_rx_ctl    => eth_rx_ctl,
                rgmii_eth_rx_data   => eth_rx_data,
                rgmii_eth_tx_ctl    => eth_tx_ctl,
                rgmii_eth_tx_data   => eth_tx_data,
                wishbone_adr        => wb_eth_adr,
                wishbone_dat_w      => wb_ext_io_in.dat,
                wishbone_dat_r      => wb_eth_out.dat,
                wishbone_sel        => wb_ext_io_in.sel,
                wishbone_cyc        => wb_eth_cyc,
                wishbone_stb        => wb_ext_io_in.stb,
                wishbone_ack        => wb_eth_out.ack,
                wishbone_we         => wb_ext_io_in.we,
                wishbone_cti        => "000",
                wishbone_bte        => "00",
                wishbone_err        => open,
                interrupt           => ext_irq_eth
                );

        -- Gate cyc with "chip select" from soc
        wb_eth_cyc <= wb_ext_io_in.cyc and wb_ext_is_eth;

        -- Remove top address bits as liteeth decoder doesn't know about them
        wb_eth_adr <= x"000" & "000" & wb_ext_io_in.adr(14 downto 0);

        -- LiteETH isn't pipelined
        wb_eth_out.stall <= not wb_eth_out.ack;

    end generate;

    no_liteeth : if not USE_LITEETH generate
        ext_irq_eth    <= '0';
    end generate;

    -- SPI Flash
    --
    spi_flash_cs_n   <= spi_cs_n;
    spi_flash_mosi   <= spi_sdat_o(0) when spi_sdat_oe(0) = '1' else 'Z';
    spi_flash_miso   <= spi_sdat_o(1) when spi_sdat_oe(1) = '1' else 'Z';
    spi_flash_wp_n   <= spi_sdat_o(2) when spi_sdat_oe(2) = '1' else 'Z';
    spi_flash_hold_n <= spi_sdat_o(3) when spi_sdat_oe(3) = '1' else 'Z';
    spi_sdat_i(0)    <= spi_flash_mosi;
    spi_sdat_i(1)    <= spi_flash_miso;
    spi_sdat_i(2)    <= spi_flash_wp_n;
    spi_sdat_i(3)    <= spi_flash_hold_n;

    uclk: USRMCLK port map (
        USRMCLKI => spi_sck,
        USRMCLKTS => '0'
        );

    has_tercel : if USE_TERCEL generate

        component tercel_core port (
            sys_clk_freq        : in std_ulogic_vector(31 downto 0);

            peripheral_clock    : in std_ulogic;
            peripheral_reset    : in std_ulogic;

            spi_clock           : out std_ulogic;
            spi_d_out           : out std_ulogic_vector(3 downto 0);
            spi_d_direction     : out std_ulogic_vector(3 downto 0);
            spi_d_in            : in std_ulogic_vector(3 downto 0);
            spi_ss_n            : out std_ulogic;

            wishbone_adr        : in std_ulogic_vector(29 downto 0);
            wishbone_dat_w      : in std_ulogic_vector(31 downto 0);
            wishbone_dat_r      : out std_ulogic_vector(31 downto 0);
            wishbone_sel        : in std_ulogic_vector(3 downto 0);
            wishbone_cyc        : in std_ulogic;
            wishbone_stb        : in std_ulogic;
            wishbone_ack        : out std_ulogic;
            wishbone_we         : in std_ulogic;
            wishbone_err        : out std_ulogic;

            cfg_wishbone_adr    : in std_ulogic_vector(29 downto 0);
            cfg_wishbone_dat_w  : in std_ulogic_vector(31 downto 0);
            cfg_wishbone_dat_r  : out std_ulogic_vector(31 downto 0);
            cfg_wishbone_sel    : in std_ulogic_vector(3 downto 0);
            cfg_wishbone_cyc    : in std_ulogic;
            cfg_wishbone_stb    : in std_ulogic;
            cfg_wishbone_ack    : out std_ulogic;
            cfg_wishbone_we     : in std_ulogic;
            cfg_wishbone_err    : out std_ulogic
            );
        end component;

        signal wb_tercel_cyc     : std_ulogic;

    begin
        tercel :  tercel_core
            port map(
                sys_clk_freq        => std_logic_vector(to_unsigned(CLK_FREQUENCY, 32)),

                peripheral_clock    => system_clk,
                peripheral_reset    => soc_rst,

                spi_clock           => spi_sck,
                spi_d_out           => spi_sdat_o,
                spi_d_direction     => spi_sdat_oe,
                spi_d_in            => spi_sdat_i,
                spi_ss_n            => spi_cs_n,

                wishbone_adr        => wb_spiflash_in.adr,
                wishbone_dat_w      => wb_spiflash_in.dat,
                wishbone_dat_r      => wb_spiflash_out.dat,
                wishbone_sel        => wb_spiflash_in.sel,
                wishbone_cyc        => wb_spiflash_in.cyc,
                wishbone_stb        => wb_spiflash_in.stb,
                wishbone_ack        => wb_spiflash_out.ack,
                wishbone_we         => wb_spiflash_in.we,
                wishbone_err        => open,

                cfg_wishbone_adr    => wb_ext_io_in.adr,
                cfg_wishbone_dat_w  => wb_ext_io_in.dat,
                cfg_wishbone_dat_r  => wb_tercel_out.dat,
                cfg_wishbone_sel    => wb_ext_io_in.sel,
                cfg_wishbone_cyc    => wb_tercel_cyc,
                cfg_wishbone_stb    => wb_ext_io_in.stb,
                cfg_wishbone_ack    => wb_tercel_out.ack,
                cfg_wishbone_we     => wb_ext_io_in.we,
                cfg_wishbone_err    => open
                );

        -- Gate cyc with "chip select" from soc
        wb_tercel_cyc <= wb_ext_io_in.cyc and wb_ext_is_tercel;

        -- Tercel isn't pipelined
        wb_tercel_out.stall <= not wb_tercel_out.ack;

    end generate;

    -- LPC slave
    --
    lpc_sframe_n   <= lpc_slave_frame_n;
    lpc_sreset_n   <= lpc_slave_reset_n;
    lpc_sclock     <= lpc_slave_clock;

    lpc_sdat_buf_0: aquila_lpc_sdr_tristate port map (
        o   => lpc_sdat_o(0),
        oe  => lpc_slave_oe,
        i   => lpc_sdat_i(0),
        clk => lpc_sclock,
        p   => lpc_slave_data(0)
        );

    lpc_sdat_buf_1: aquila_lpc_sdr_tristate port map (
        o   => lpc_sdat_o(1),
        oe  => lpc_slave_oe,
        i   => lpc_sdat_i(1),
        clk => lpc_sclock,
        p   => lpc_slave_data(1)
        );

    lpc_sdat_buf_2: aquila_lpc_sdr_tristate port map (
        o   => lpc_sdat_o(2),
        oe  => lpc_slave_oe,
        i   => lpc_sdat_i(2),
        clk => lpc_sclock,
        p   => lpc_slave_data(2)
        );

    lpc_sdat_buf_3: aquila_lpc_sdr_tristate port map (
        o   => lpc_sdat_o(3),
        oe  => lpc_slave_oe,
        i   => lpc_sdat_i(3),
        clk => lpc_sclock,
        p   => lpc_slave_data(3)
        );

    lpc_sirq_buf: aquila_lpc_sdr_tristate port map (
        o   => lpc_sirq_o,
        oe  => lpc_sirq_oe,
        i   => lpc_sirq_i,
        clk => lpc_sclock,
        p   => lpc_slave_serirq
        );

    has_lpc_slave : if USE_LPC_SLAVE generate

        component aquila_lpc_slave_wishbone port (
            peripheral_clock      : in std_ulogic;
            peripheral_reset      : in std_ulogic;

            lpc_data_out          : out std_ulogic_vector(3 downto 0);
            lpc_data_in           : in std_ulogic_vector(3 downto 0);
            lpc_data_direction    : out std_ulogic;
            lpc_irq_out           : out std_ulogic;
            lpc_irq_in            : in std_ulogic;
            lpc_irq_direction     : out std_ulogic;

            lpc_frame_n           : in std_ulogic;
            lpc_reset_n           : in std_ulogic;
            lpc_clock             : in std_ulogic;

            slave_wishbone_adr    : in std_ulogic_vector(29 downto 0);
            slave_wishbone_dat_w  : in std_ulogic_vector(31 downto 0);
            slave_wishbone_dat_r  : out std_ulogic_vector(31 downto 0);
            slave_wishbone_sel    : in std_ulogic_vector(3 downto 0);
            slave_wishbone_cyc    : in std_ulogic;
            slave_wishbone_stb    : in std_ulogic;
            slave_wishbone_ack    : out std_ulogic;
            slave_wishbone_we     : in std_ulogic;
            slave_wishbone_err    : out std_ulogic;

            master_wishbone_adr   : out std_ulogic_vector(wishbone_addr_bits-1 downto 0);
            master_wishbone_dat_w : out std_ulogic_vector(wishbone_data_bits-1 downto 0);
            master_wishbone_dat_r : in std_ulogic_vector(wishbone_data_bits-1 downto 0);
            master_wishbone_sel   : out std_ulogic_vector(wishbone_sel_bits-1 downto 0);
            master_wishbone_cyc   : out std_ulogic;
            master_wishbone_stb   : out std_ulogic;
            master_wishbone_ack   : in std_ulogic;
            master_wishbone_we    : out std_ulogic;
            master_wishbone_err   : in std_ulogic;

            debug_port            : out std_ulogic_vector(15 downto 0);
            lpc_clock_mirror      : out std_ulogic
            );
        end component;

        signal wb_lpc_slave_cyc   : std_ulogic;

    begin
        lpc_slave :  aquila_lpc_slave_wishbone
            port map(
                peripheral_clock    => system_clk,
                peripheral_reset    => soc_rst,

                lpc_data_out           => lpc_sdat_o,
                lpc_data_in            => lpc_sdat_i,
                lpc_data_direction     => lpc_slave_oe,
                lpc_irq_out            => lpc_sirq_o,
                lpc_irq_in             => lpc_sirq_i,
                lpc_irq_direction      => lpc_sirq_oe,
                lpc_frame_n            => lpc_sframe_n,
                lpc_reset_n            => lpc_sreset_n,
                lpc_clock              => lpc_sclock,

                master_wishbone_adr    => wb_master_lpc_slave_out.adr,
                master_wishbone_dat_w  => wb_master_lpc_slave_out.dat,
                master_wishbone_dat_r  => wb_master_lpc_slave_in.dat,
                master_wishbone_sel    => wb_master_lpc_slave_out.sel,
                master_wishbone_cyc    => wb_master_lpc_slave_out.cyc,
                master_wishbone_stb    => wb_master_lpc_slave_out.stb,
                master_wishbone_ack    => wb_master_lpc_slave_in.ack,
                master_wishbone_we     => wb_master_lpc_slave_out.we,
                master_wishbone_err    => '0',

                slave_wishbone_adr    => wb_ext_io_in.adr,
                slave_wishbone_dat_w  => wb_ext_io_in.dat,
                slave_wishbone_dat_r  => wb_lpc_slave_out.dat,
                slave_wishbone_sel    => wb_ext_io_in.sel,
                slave_wishbone_cyc    => wb_lpc_slave_cyc,
                slave_wishbone_stb    => wb_ext_io_in.stb,
                slave_wishbone_ack    => wb_lpc_slave_out.ack,
                slave_wishbone_we     => wb_ext_io_in.we,
                slave_wishbone_err    => open,

                debug_port            => open,
                lpc_clock_mirror      => open
                );

        -- Gate cyc with "chip select" from soc
        wb_lpc_slave_cyc <= wb_ext_io_in.cyc and wb_ext_is_lpc_slave;

        -- Aquila isn't pipelined
        wb_lpc_slave_out.stall <= not wb_lpc_slave_out.ack;

    end generate;

    -- Mux WB response on the IO bus
    wb_ext_io_out <= wb_eth_out when wb_ext_is_eth = '1' else
                     wb_tercel_out when wb_ext_is_tercel = '1' else
                     wb_lpc_slave_out when wb_ext_is_lpc_slave = '1' else
                     wb_dram_ctrl_out;

end architecture behaviour;
